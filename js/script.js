let arr = [true, null, "1", "2", ["2.1", "2.2",["2.2.1", "2.2.2",["2.2.2.1", "2.2.2.2"]]], 3];
debugger;

let parent = document.querySelector("#parent");
renderList(parent, arr);
function renderList(parent, items) {
    let newUl = document.createElement("ul");
    for (let item of items) {
        if (item == null) {
            continue;
        }
        if (typeof item === "string"||  typeof item === "number"  || typeof item === "boolean") {
            newUl.append(creatLi(item));
        } else if (typeof item === "object") {
            renderList(newUl, item);
        }
    }
    parent.append(newUl);
}

function creatLi(text) {
    let newLi = document.createElement("li");
    newLi.innerText = text;
    return newLi;
}